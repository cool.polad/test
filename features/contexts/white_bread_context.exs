defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers
  alias Auction.{Repo, Auction, AuctionAPIController, User}

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    Ecto.Adapters.SQL.Sandbox.checkout(Repo)
    Ecto.Adapters.SQL.Sandbox.mode(Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    Ecto.Adapters.SQL.Sandbox.checkin(Repo)
    # Hound.end_session
  end

  given_ ~r/^the following auctions$/, fn state, %{table_data: table} ->
    table = table
    |> Enum.map(fn auction_data -> Map.put_new(auction_data, :closing_date, NaiveDateTime.add(AuctionAPIController.getCurrentNaiveDateTime(), 3*86400)) end)
    |> Enum.map(fn auction_data -> Auction.changeset(%Auction{}, auction_data) end)
    |> Enum.map(fn changeset -> Repo.insert!(changeset) end)

    [%{username: "frodo", password: "parool"},
     %{username: "bilbo", password: "parool"}]
    |> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)

    navigate_to "/"

    {:ok, Map.put_new(state, :table_data, table)}
  end

  then_ ~r/^User see welcome page and proceed to the auctions page$/, fn state ->
    # navigate_to "/#/auction"
    click({:id, "start-auction"})

    {:ok, state}
  end

  then_ ~r/^User selects the auction "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: argument_one} ->
    %{table_data: table} = state
    elem = Enum.filter(table, fn auc -> Map.get(auc, :description) == argument_one end) |> List.first
    click({:id, "elem-#{elem.id}"})

    {:ok, state}
  end

  and_ ~r/^Enters a new bid in amount "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: argument_one} ->
    fill_field({:id, "offeredBid"}, argument_one)
    click({:class, "btn-info"})

    {:ok, state}
  end

  and_ ~r/^current bid is accepted$/, fn state ->
    assert visible_in_page? ~r/Accepted/

    {:ok, state}
  end

  then_ ~r/^User goes back to do new bid for "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: argument_one} ->
    click({:id, "btn-back"})

    %{table_data: table} = state
    elem = Enum.filter(table, fn auc -> Map.get(auc, :description) == argument_one end) |> List.first
    click({:id, "elem-#{elem.id}"})

    {:ok, state}
  end

  and_ ~r/^Enters a new bid with price "(?<argument_one>[^"]+)"$/,
  fn state, %{argument_one: argument_one} ->
    fill_field({:id, "offeredBid"}, argument_one)
    click({:class, "btn-info"})

    {:ok, state}
  end

  and_ ~r/^current bid is rejected$/, fn state ->
    assert visible_in_page? ~r/Denied/
    Process.sleep(2000)
    {:ok, state}
  end
end
