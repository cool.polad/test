Feature: Auction bidding
  As an automated system (STRS)
  Such that Auctions could be bidded
  I want to make a new bid in auction by selecting corresponding auctions from the given list
  Scenario: Bidding via web brow
    Given the following auctions
          | description                                 | price |
          | Chicago Bullet Speed Skate (size 7)         | 84.0  |
          | Riedell Dart Derby Skates (size 8)          | 106.0 |
          | Roller Derby Bounded Blonde Skates (size 7) | 112.0 |
    Then User see welcome page and proceed to the auctions page
    Then User selects the auction "Chicago Bullet Speed Skate (size 7)"
    And Enters a new bid in amount "200.00"
    And current bid is accepted
    Then User goes back to do new bid for "Riedell Dart Derby Skates (size 8)"
    And Enters a new bid with price "50.00"
    And current bid is rejected
