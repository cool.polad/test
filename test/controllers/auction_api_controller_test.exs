defmodule Auction.AuctionAPIControllerTest do
  use Auction.ConnCase
  alias Auction.{Auction, Repo}

  test "Get all auctions", %{conn: conn} do
    [%{description: "Chicago Bullet Speed Skate (size 7)", price: 84.0},
     %{description: "Riedell Dart Derby Skates (size 8)", price: 106.0},
     %{description: "Roller Derby Bounded Blonde Skates (size 7)", price: 112.0}]
    |> Enum.map(fn auction_data -> Auction.changeset(%Auction{}, auction_data) end)
    |> Enum.map(fn changeset -> Repo.insert!(changeset) end)

    conn = conn |> get(auction_api_path(conn, :index))
    assert Map.get(conn, :resp_body) =~ ~r/List of Auctions/
  end

  test "Get one of auctions", %{conn: conn} do
    auctions =
      [%{description: "Chicago Bullet Speed Skate (size 7)", price: 84.0},
       %{description: "Riedell Dart Derby Skates (size 8)", price: 106.0},
       %{description: "Roller Derby Bounded Blonde Skates (size 7)", price: 112.0}]
      |> Enum.map(fn auction_data -> Auction.changeset(%Auction{}, auction_data) end)
      |> Enum.map(fn changeset -> Repo.insert!(changeset) end)
    auction = List.first(auctions)
    conn = conn |> get(auction_api_path(conn, :show, auction.id))
    assert Map.get(conn, :resp_body) =~ ~r/Single Auction/
  end

  test "Accept a bid", %{conn: conn} do
    auction = %{description: "Chicago Bullet Speed Skate (size 7)", price: 84.0}
    auction =
    Auction.changeset(%Auction{}, auction)
    |> Repo.insert!
    conn = conn |> patch(auction_api_path(conn, :update, auction.id), new_bid: 120.0)
    assert Map.get(conn, :resp_body) =~ ~r/Accepted/
  end

  test "Reject a bid", %{conn: conn} do
    auction = %{description: "Chicago Bullet Speed Skate (size 7)", price: 84.0}
    auction =
    Auction.changeset(%Auction{}, auction)
    |> Repo.insert!
    conn = conn |> patch(auction_api_path(conn, :update, auction.id), new_bid: 50.0)
    assert Map.get(conn, :resp_body) =~ ~r/Not Modified/
  end

end
