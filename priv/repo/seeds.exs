alias Auction.{Repo, Auction, AuctionAPIController, User}

[%{description: "Chicago Bullet Speed Skate (size 7)", price: 84.0},
 %{description: "Riedell Dart Derby Skates (size 8)", price: 106.0},
 %{description: "Roller Derby Bounded Blonde Skates (size 7)", price: 112.0}]
|> Enum.map(fn auction_data -> Map.put_new(auction_data, :closing_date, NaiveDateTime.add(AuctionAPIController.getCurrentNaiveDateTime(), 3*86400)) end)
|> Enum.map(fn auction_data -> Auction.changeset(%Auction{}, auction_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)

[%{username: "frodo", password: "parool"},
 %{username: "bilbo", password: "parool"}]
|> Enum.map(fn user_data -> User.changeset(%User{}, user_data) end)
|> Enum.each(fn changeset -> Repo.insert!(changeset) end)
