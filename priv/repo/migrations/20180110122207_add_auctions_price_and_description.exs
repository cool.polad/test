defmodule Auction.Repo.Migrations.AddAuctionsPriceAndDescription do
  use Ecto.Migration

  def change do
    create table(:auctions) do
      add :price, :float
      add :description, :string

      timestamps()
    end
  end
end
