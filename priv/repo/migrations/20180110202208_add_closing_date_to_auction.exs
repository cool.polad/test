defmodule Auction.Repo.Migrations.AddClosingDateToAuction do
  use Ecto.Migration

  def change do
    alter table(:auctions) do
      add :closing_date, :naive_datetime
    end
  end
end
