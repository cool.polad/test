defmodule Auction.Repo.Migrations.AddBidsListForAuctions do
  use Ecto.Migration

  def change do
    create table(:bids) do
      add :price, :float
      add :auction_id, references(:auctions)

      timestamps()
    end
  end
end
