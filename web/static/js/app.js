import "phoenix_html";
import "axios";
import Vue from "vue";
import VueRouter from "vue-router";
import auctions from "./auctions";
import auction from "./auction";
import main from "./main";

Vue.use(VueRouter);

var router = new VueRouter({
    routes:[
        // {path: '/login', component: login, beforeEnter: afterAuth},
        {path: '/', component: main, name: 'main'},
        {path: '/auctions', component: auctions, name: 'auctions'},
        {path: '/auction', component: auction, name: 'auction'},
        {path: '*', redirect: '/'}
    ]
});
new Vue({ router }).$mount("#auctions-app");

// Vue.component("auctions", auctions);
