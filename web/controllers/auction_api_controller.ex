defmodule Auction.AuctionAPIController do
  use Auction.Web, :controller
  alias Auction.{Auction, Repo, User}

  def index(conn, _params) do
    auctions =
      Repo.all(Auction)
      |> Repo.preload(bids: [:user])
      |> Enum.filter(fn auction -> NaiveDateTime.compare(auction.closing_date, getCurrentNaiveDateTime) == :gt end)
      |> Enum.map(fn auction -> convertStructToMap(auction) end)

    # IO.inspect auctions
    conn
    |> put_status(200)
    |> json(%{msg: "List of Auctions", auctions: auctions})
  end

  def show(conn, %{"id" => auction_id}) do
    auction =
      Repo.get!(Auction, auction_id)
      |> Repo.preload(bids: [:user])
      |> convertStructToMap
    # IO.inspect auction
    conn
    |> put_status(200)
    |> json(%{msg: "Single Auction", auction: auction})
  end


  def update(conn, %{"id" => auction_id, "new_bid" => new_bid}) do
    user = Repo.all(User) |> List.first()
    new_bid = Float.parse(new_bid) |> elem(0)
    auction = Repo.get!(Auction, auction_id) |> Repo.preload(:bids)
    max_bid = Enum.max_by(auction.bids, fn bid_map -> Map.get(bid_map, :price) end, fn -> %{price: auction.price} end)
              |> Map.fetch(:price)
              |> elem(1)
    if new_bid>max_bid || new_bid == auction.price do
      bid_with_auction_rel = Ecto.build_assoc(auction, :bids, %{price: new_bid})
      Ecto.build_assoc(user, :bids, bid_with_auction_rel)
      |> Repo.insert!
      conn
      |> put_status(202)
      |> json(%{msg: "Accepted"})
    else
      conn
      |> put_status(304)
      |> json(%{msg: "Not Modified. Offered bid is less than #{max_bid}."})
    end
  end

  def convertStructToMap(struct) do
     struct = Map.from_struct(struct)
     |> Map.delete(:__meta__)

     if Map.has_key?(struct, :auction) do
       struct = Map.delete(struct, :auction)
     end

     if Map.has_key?(struct, :user) do
       struct = Map.update(struct, :user, %{username: "undefined", id: -1}, fn user -> %{username: user.username, id: user.id} end)
     end

     if Map.has_key?(struct, :bids) && length(Map.get(struct, :bids)) > 0 do
       Map.update!(struct, :bids, fn bids_list -> Enum.map(bids_list, fn bid -> convertStructToMap(bid) end) end)
     else
       struct
     end
  end

  def getCurrentNaiveDateTime() do
    {{year, month, day}, {hour, minute, second}} = :calendar.local_time()
    current_datetime = NaiveDateTime.new(year, month, day, hour, minute, second) |> elem(1)
  end
end
