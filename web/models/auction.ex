defmodule Auction.Auction do
  use Auction.Web, :model

  schema "auctions" do
    field :price, :float
    field :description, :string
    field :closing_date, :naive_datetime
    has_many :bids, Auction.Bid
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:description, :price, :closing_date])
    |> validate_required([:description, :price, :closing_date])
  end

end
