defmodule Auction.Bid do
  use Auction.Web, :model

  schema "bids" do
    field :price, :float
    belongs_to :auction, Auction.Auction
    belongs_to :user, Auction.User
    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:price])
    |> validate_required([:price])
  end

end
